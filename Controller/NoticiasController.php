<?php
class NoticiasController extends SitesAppController {
	
	public $uses = array( 'Portal.Noticia' );
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}
	public function index() {
	}
	
	public function view($noticia_id = null) {		
		$this->set('title_for_layout','Noticia');
		
		$this->Noticia->Behaviors->attach('Containable');
		$this->Noticia->contain(
			'Thumb',
			'Thumb.Imagem'
		);
		$Noticia = $this->Noticia->read(null, $noticia_id);
		
		$this->set('Noticia', $Noticia);
		
	}
}