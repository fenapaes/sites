<?php

App::uses('Controller', 'Controller');

class SitesAppController extends AppController {
	
	public $helpers = array(
		'Bootstrap.Bootstrap'
	);
	
	public $uses = array( 'Portal.Site' );
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		
		// Configura qual site está sendo acessado
		$server_name = split('\.', $_SERVER['HTTP_HOST']);
		$site_name = $server_name[0];
		
		if ($site_name == 'apaebr') {
			$site_name = 'fenapaes';
		}
		if ($site_name == 'fenapaes') {
			$entidade_tipo = 'fenapaes';
		} else {
			$entidade_tipo = 'apaes';
		}
		$conditions = array(
			'Site.dominio' => $site_name	
		);
		$Site = $this->Site->find('first', array('conditions'=>$conditions));
				
		$this->SiteAtual = $Site;
		$this->layout = $Site['Template']['arquivo'];
		
		
		$this->set('Site', $Site);

		$this->set('server_name', $server_name);
		$this->set('site_name', $site_name);
		$this->set('entidade_tipo', $entidade_tipo);
		
		$menus = array(
			array(
				'Menu' => array(
					'title' => 'Menu Fenapaes'
				),
				'Links' => array(
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Fenderação Nacional das Apaes',
							'plugin' => 'Sites',
							'controller' => 'home',
							'action' => 'index',
							'icon' => '',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Filiação',
							'plugin' => 'Sites',
							'controller' => 'filiacoes',
							'action' => 'index',
							'icon' => '',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Eventos',
							'plugin' => 'Sites',
							'controller' => 'eventos',
							'action' => 'index',
							'icon' => '',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Mapa',
							'plugin' => 'Sites',
							'controller' => 'mapa',
							'action' => 'index',
							'icon' => '',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Agenda Presidencial',
							'plugin' => 'Sites',
							'controller' => 'agenda_presidencial',
							'action' => 'index',
							'icon' => '',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Fale com a Presidente',
							'plugin' => 'Sites',
							'controller' => 'fale_presidente',
							'action' => 'index',
							'icon' => 'glyphicon glyphicon-envelope',
							'class'=>''
						)
					),
					array(
						'Link' => array(
							'id' => 1,
							'text' => 'Portal',
							'plugin' => 'portal',
							'controller' => 'Status',
							'action' => 'index',
							'icon' => 'glyphicon glyphicon-tasks',
							'class'=>'pull-right'
						)
					)
				)
			)
		);
		$this->set('menus', $menus);
	}

}
