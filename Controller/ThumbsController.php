<?php
class ThumbsController extends SitesAppController {
	
	public $uses = array( 'Portal.Thumb' );
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}
	public function index() {
		
	}
	
	public function thumber($Thumb) {
		//pr($Thumb);
		$arquivo_src = WWW_ROOT.'imagens/'.$Thumb['Imagem']['site_id'].'/'.$Thumb['Imagem']['imagem'];
		$arquivo_dst = WWW_ROOT.'/imagens/'.$Thumb['Imagem']['site_id'].'/'.$Thumb['Thumb']['largura'].'_'.$Thumb['Thumb']['altura'].'_'.$Thumb['Imagem']['imagem'];
		//if (!is_file($arquivo_dst)) {
			list($width, $height) = getimagesize($arquivo_src);
			$new_thumb = imagecreatetruecolor($Thumb['Thumb']['largura'], $Thumb['Thumb']['altura']);
			if ($Thumb['Imagem']['ImagensTipo']['nome'] == 'jpg') {
				$original = imagecreatefromjpeg($arquivo_src);
				
			}
			imagecopyresampled($new_thumb, $original, 0, 0, 0, 0, $Thumb['Thumb']['largura'], $Thumb['Thumb']['altura'], $width, $height);
			header('Content-Type: image/jpeg');
			imagejpeg($new_thumb, null, 90);
		//}
	}
	
	public function view($thumb_id = false) {
		$this->layout = false;
		$this->Thumb->Behaviors->attach('Containable');
		$this->Thumb->contain('Imagem','Imagem.ImagensTipo');
		$Thumb = $this->Thumb->read(null, $thumb_id);
		$this->thumber($Thumb);
		//echo '/imagens/'.$Thumb['Imagem']['site_id'].'/'.$Thumb['Thumb']['largura'].'_'.$Thumb['Thumb']['altura'].'_'.$Thumb['Imagem']['imagem'];
		$this->render(false);
	}
}