<?php
class HomeController extends SitesAppController {
	
	public $uses = array( 'Portal.Noticia', 'Portal.Banner' );
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index');
	}
	public function index() {
		
		$this->set('title_for_layout','Bem-vindo!');
		$site_id = $this->SiteAtual['Site']['id'];
		
		$conditions = array(
			'Noticia.site_id' => $site_id
		);
		$this->Noticia->Behaviors->attach('Containable');
		$this->Noticia->contain(
			'Thumb',
			'Thumb.Imagem'
		);
		$Noticias = $this->Noticia->find('all', array('conditions'=>$conditions));
		
		$this->set('Noticias', $Noticias);
		
		// Banner Lateral
		$conditions = array(
			'Banner.site_id' => $site_id,
			'Banner.banners_posicao_id' => 2
		);
		$this->Banner->Behaviors->attach('Containable');
		$this->Banner->contain(
			'Thumb',
			'Thumb.Imagem'
		);
		$BannersL = $this->Banner->find('all', array('conditions'=>$conditions));
		
		$this->set('BannersL', $BannersL);
		
		// Banner Superior
		$conditions = array(
			'Banner.site_id' => $site_id,
			'Banner.banners_posicao_id' => 1
		);
		$this->Banner->Behaviors->attach('Containable');
		$this->Banner->contain(
			'Thumb',
			'Thumb.Imagem'
		);
		$BannersS = $this->Banner->find('all', array('conditions'=>$conditions));
		
		$this->set('BannersS', $BannersS);
		
	}
}