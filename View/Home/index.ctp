<?php $this->extend('Templates/'.$entidade_tipo.'/home-page'); // Extende o template de home ?>

<?php $this->start('css'); ?>
<?php echo $this->Html->css('Sites.sites'); // Carrega estilos  ?>
<?php $this->end(); ?>

<?php // Banners da Entidade ?>
<?php $this->start('banner-large'); // Banners grandes da Home ?>
	<ul class="media-list">
		<li class="media"><img class="img-rounded" width="100%" src="/sites/Thumbs/view/<?php echo $BannersS[0]['Thumb']['id']; ?>"></li>
	</ul>
<?php $this->end(); ?>

<?php $this->start('banner-side'); // Banners grandes da Home ?>
	<ul class="media-list">
		<li class="media"><img class="img-rounded" width="100%" src="/sites/Thumbs/view/<?php echo $BannersL[0]['Thumb']['id']; ?>"></li>
	</ul>
<?php $this->end(); ?>

<?php // Noticias da Entidade ?>

<?php $this->start('news'); ?>
<?php // News ?>
<div class="row">
	<?php foreach($Noticias as $Noticia) { ?>
	<div class="col-md-6">
		<div id="news_<?php echo $Noticia['Noticia']['id'];?>" class="panel panel-info">
			<div class="panel-heading">
				<h4 class="media-heading"><strong><?php echo $Noticia['Noticia']['data_noticia']; ?></strong> - <?php echo $Noticia['Noticia']['titulo']; ?> </h4>
			</div>
			<div class="panel-body">
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object img-thumbnail" src="/sites/Thumbs/view/<?php echo $Noticia['Thumb']['id'];?>">
					</a>
					<div class="media-body">
						<?php echo substr($Noticia['Noticia']['corpo'],0,100); ?>
						<?php echo $this->Html->link('Ler mais...', array('controller'=>'noticias','action'=>'view', $Noticia['Noticia']['id'])); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php $this->end(); ?>


