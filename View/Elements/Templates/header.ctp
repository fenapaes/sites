<div class="row">
	<div class="col-md-6">
		<?php if ($this->fetch('logo')): ?>
		<div>
			<div class="panel-body">
				<?php echo $this->fetch('logo'); ?>
			</div>
		</div>
		<?php else: ?>
		<div class="alert alert-danger">Necessário bloco "logo" aqui!</div>
		<?php endif; // logo ?>
	</div>
	<div class="col-md-6">
		<?php if ($this->fetch('social')): ?>
		<div>
			<div class="panel-body">
				<?php echo $this->fetch('social'); ?>
			</div>
		</div>
		<?php else: ?>
		<div class="alert alert-danger">Necessário bloco "social" aqui!</div>
		<?php endif; // social ?>
	</div>
</div>
