<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $title_for_layout; ?></title>
		<?php
		echo $this->Html->meta( 'icon' );
		// jQuery
		echo $this->Html->script( 'Bootstrap./js/jquery-2.1.0.min'); // no IE6
		// Bootstrap Datepicker
        echo $this->Html->script( 'Bootstrap.bootstrap-datepicker' );
        echo $this->Html->css( 'Bootstrap.datepicker' );
        // MaskedInput
        echo $this->Html->script( 'Bootstrap.jquery.maskedinput.min' );
		// HotKeys
		// echo $this->Html->script( 'Bootstrap.jquery.hotkeys' ); // Hotkeys
		// Bootstrap
		echo $this->Html->css( 'Bootstrap./bootstrap/css/bootstrap.min' ); // CSS
		echo $this->Html->script( 'Bootstrap./bootstrap/js/bootstrap.min' ); // JS
		// Portal Fenapaes
		echo $this->Html->script( 'Sites.portal' ); // CSS do Portal FENAPAES
		echo $this->fetch( 'meta' ); // De Views
		echo $this->fetch( 'css' ); // De Views
		echo $this->fetch( 'script' ); // De Views
		// Bootstrap
		echo $this->Html->css('Bootstrap.authbootstrap.min');
		echo $this->Html->script( 'Bootstrap.authbootstrap' );
		
		echo $this->Html->css('Sites.sites');
		?>
	</head>
	<body>
		<?php
		//echo $this->Element( 'Sites.apaes-navbar-top' ); // Menu Superior FENAPAES
		?>
		<div class="hide" id="doc-body">
			<div class="container">
				<?php
				echo $this->Session->flash( 'flash', array( 'element'=>'Bootstrap.flash' ) ); // Mensagens da Sessao
				echo $this->Element('Templates/header'); // Cabeçalho
				echo $this->Element('Templates/menu-local'); // Menu Superior Local
				echo $this->fetch( 'content' ); // Conteúdo Principal
				?>
			</div>
			<?php
			echo $this->Element( 'Bootstrap.navbar-bottom' ); // Navegação Inferior
			?>
		</div>
		<?php echo $this->Element('sql_dump'); // Dump SQL do CakePHP ?>
	</body>
</html>
