<?php echo $this->fetch('css'); // Arquivos CSS ?>
<?php echo $this->fetch('script'); // Arquivos javascript ?>

<?php // Logotipo ?>
<?php $this->start('logo'); ?>

	<img src="/logos/<?php echo $Site['Site']['id'];?>.jpg">

<?php $this->end(); // Logo?>
	
<?php // Icones Sociais da Entidade ?>
<?php $this->start('social'); ?>
	&nbsp;<img class="pull-right social-icon" width="32" src="http://static.iconarchive.com/static/images/social/twitter-icon.png">
	&nbsp;<img class="pull-right social-icon" src="http://static.iconarchive.com/static/images/social/google-plus-icon.png">
	&nbsp;<img class="pull-right social-icon" src="http://static.iconarchive.com/static/images/social/facebook-icon.png">
<?php $this->end(); // Social ?>

<?php // Menu Local da Entidade ?>
<?php $this->start('header'); ?>
	<?php // Menu ?>
	<div class="btn-group">
		<a href="#" class="btn btn-primary">O que fazemos?</a>&nbsp;
		<a href="#" class="btn btn-info">Semana Nacional</a>&nbsp;
		<a href="#" class="btn btn-success">Saiba como Ajudar</a>&nbsp;
		<a href="#" class="btn btn-danger">Contato</a>&nbsp;
		<a href="#" class="btn btn-warning">Coordenadores Nacionais</a>&nbsp;
	</div>
	<?php // Formulário de Pesquisa ?>
	<form id="form-search" class="pull-right form-inline" role="form" >
		<div class="form-group">
			<label class="sr-only" for="pesquise">Pesquise</label>
			<input type="text" class="form-control" id="pesquise" placeholder="Pesquise...">
		</div>
	</form>
	<br><br>
<?php $this->end(); ?>

<?php // Todo o resto ?>
<?php echo $this->fetch( 'content' ); ?>