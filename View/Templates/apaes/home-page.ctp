<?php $this->extend('Templates/'.$entidade_tipo.'/layout'); ?>
<div class="row">
	<div class="col-md-12">
		<div>
		<?php // Banner principal ?>
		<?php echo $this->fetch('banner-large'); ?>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-8">
		<ul class="media-list">
		<?php // Notícias ?>
		<?php echo $this->fetch('news'); ?>
		</ul>
	</div>
	<?php // Menu Lateral ?>
	<div class="col-md-4">
		<?php // Banner Lateral ?>
		<?php echo $this->fetch('banner-side'); ?>
		<br><br>
		<iframe class="img-rounded" style="padding: 4px; background-color: #000;" width="320" height="210" src="//www.youtube.com/embed/cbK_EJG9cvE?list=UUA1MtHJvzFbgw3Vih-uE5uQ" frameborder="0" allowfullscreen></iframe>
	</div>
</div>
<?php echo $this->Element('Templates/eventos-home'); ?>
